package nl.enschedelly.users.exception;

import org.springframework.http.HttpStatus;

public class IncorrectCredentialsException extends ResponseException {

    public IncorrectCredentialsException() {
        super(HttpStatus.UNAUTHORIZED, "Incorrect email or password");
    }
}
