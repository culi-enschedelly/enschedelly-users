package nl.enschedelly.users.exception;

import org.springframework.http.HttpStatus;

public class AuthenticationRequiredException extends ResponseException {

    public AuthenticationRequiredException() {
        super(HttpStatus.UNAUTHORIZED, "This resource requires authentication");
    }
}
