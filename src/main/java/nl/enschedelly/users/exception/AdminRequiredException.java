package nl.enschedelly.users.exception;

import org.springframework.http.HttpStatus;

public class AdminRequiredException extends ResponseException {

    public AdminRequiredException() {
        super(HttpStatus.FORBIDDEN, "This resource requires admin privileges");
    }
}
