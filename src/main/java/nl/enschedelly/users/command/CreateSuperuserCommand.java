package nl.enschedelly.users.command;

import nl.enschedelly.users.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Scanner;

@ShellComponent
public class CreateSuperuserCommand {

    @Autowired
    private AuthService authService;

    private final Scanner scanner;

    public CreateSuperuserCommand() {
        this.scanner = new Scanner(System.in);
    }

    @ShellMethod("Create a superuser.")
    public String createSuperuser(
        @ShellOption(defaultValue = "") String firstName,
        @ShellOption(defaultValue = "") String lastName,
        @ShellOption(defaultValue = "") String email,
        @ShellOption(defaultValue = "") String password
    ) {
        while (firstName.equals("")) {
            firstName = getInput("First name: ");
        }

        while (lastName.equals("")) {
            lastName = getInput("Last name: ");
        }

        while (email.equals("")) {
            email = getInput("Email: ");
        }

        while (password.equals("")) {
            password = getInput("Password: ");
        }

        authService.createSuperuser(firstName, lastName, email, password, true);
        return "User account successfully created";
    }

    private String getInput(String message) {
        System.out.print(message);
        return scanner.next();
    }
}

