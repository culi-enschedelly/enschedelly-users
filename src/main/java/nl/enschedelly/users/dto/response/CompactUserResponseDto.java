package nl.enschedelly.users.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import nl.enschedelly.users.model.User;

@Getter
@Setter
@Accessors(chain = true)
public class CompactUserResponseDto implements UserResponse {

    private String uuid;

    private String firstName;

    private String lastName;

    public CompactUserResponseDto(String uuid, String firstName, String lastName) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public CompactUserResponseDto() {}

    public static CompactUserResponseDto from(User user) {
        return new CompactUserResponseDto()
            .setUuid(user.getUuid())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName());
    }
}
