package nl.enschedelly.users.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import nl.enschedelly.users.model.User;

@Getter
@Setter
@Accessors(chain = true)
public class UserResponseDto implements UserResponse {

    private String uuid;

    private String firstName;

    private String lastName;

    private String email;

    private Long joinedAt;

    private Boolean isAdmin;

    public UserResponseDto(String uuid, String firstName, String lastName, String email, Long joinedAt, Boolean isAdmin) {
        this. uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.joinedAt = joinedAt;
        this.isAdmin = isAdmin;
    }

    public UserResponseDto() {}

    public static UserResponseDto from(User user) {
        return new UserResponseDto()
            .setUuid(user.getUuid())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName())
            .setEmail(user.getEmail())
            .setJoinedAt(user.getJoinedAt())
            .setIsAdmin(user.getIsAdmin());
    }
}
