package nl.enschedelly.users.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenResponseDto {

    public TokenResponseDto(String token) {
        this.token = token;
    }

    public TokenResponseDto() {}

    private String token;
}
