package nl.enschedelly.users.controller;

import nl.enschedelly.users.dto.request.UserRequestDto;
import nl.enschedelly.users.dto.response.UserResponse;
import nl.enschedelly.users.dto.response.UserResponseDto;
import nl.enschedelly.users.exception.AlreadyExistsException;
import nl.enschedelly.users.exception.ModelNotFoundException;
import nl.enschedelly.users.model.User;
import nl.enschedelly.users.security.RequiresAdmin;
import nl.enschedelly.users.security.RequiresAuthentication;
import nl.enschedelly.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequiresAuthentication
    @GetMapping("/me")
    public UserResponseDto me(HttpServletRequest request) {
        return UserResponseDto.from((User) request.getAttribute("user"));
    }

    @RequiresAdmin
    @GetMapping("")
    public List<? extends UserResponse> getAllUsers(
        @RequestParam(required = false, defaultValue = "false") Boolean pending,
        @RequestParam(required = false, defaultValue = "false") Boolean compact) {
            return userService.getAllUsers(pending, compact);
    }

    @RequiresAuthentication
    @GetMapping("/{uuid}")
    public UserResponseDto getUser(@PathVariable String uuid) throws ModelNotFoundException {
        return userService.getUser(uuid);
    }

    @PostMapping("")
    public UserResponseDto createUser(@RequestBody UserRequestDto request) throws AlreadyExistsException {
        return userService.createUser(request);
    }

    @RequiresAdmin
    @PostMapping("/{uuid}/approve")
    public UserResponseDto approveUser(@PathVariable String uuid) throws ModelNotFoundException {
        return userService.approveUser(uuid);
    }

    @RequiresAdmin
    @PostMapping("/{uuid}/deny")
    public void denyUser(@PathVariable String uuid) throws ModelNotFoundException {
        userService.denyUser(uuid);
    }

    @RequiresAuthentication
    @PutMapping("/{uuid}")
    public UserResponseDto updateUser(@PathVariable String uuid, @RequestBody UserRequestDto request) throws ModelNotFoundException, AlreadyExistsException {
        return userService.updateUser(uuid, request);
    }
    // TODO: verify that user can only edit himself or is admin

    @RequiresAuthentication
    @DeleteMapping("/{uuid}")
    public void deleteUser(@PathVariable String uuid) throws ModelNotFoundException {
        userService.deleteUser(uuid);
    }
    // TODO: verify that user can only edit himself or is admin
}
