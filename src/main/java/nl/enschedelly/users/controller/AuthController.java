package nl.enschedelly.users.controller;

import nl.enschedelly.users.dto.request.LoginRequestDto;
import nl.enschedelly.users.dto.response.TokenResponseDto;
import nl.enschedelly.users.exception.IncorrectCredentialsException;
import nl.enschedelly.users.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public TokenResponseDto login(@Valid @RequestBody LoginRequestDto loginRequestDto) throws IncorrectCredentialsException {
        return authService.login(loginRequestDto);
    }
}
