package nl.enschedelly.users.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import nl.enschedelly.users.model.User;
import nl.enschedelly.users.repository.UserRepository;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.PublicKey;
import java.util.Collection;
import java.util.Optional;

public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final PublicKey jwtVerifyingKey;

    private final UserRepository userRepository;

    public JwtAuthorizationFilter(PublicKey jwtVerifyingKey, UserRepository userRepository) {
        this.jwtVerifyingKey = jwtVerifyingKey;
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authHeader = request.getHeader("X-Authorization");
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            return;
        }

        String token = authHeader.substring("Bearer ".length());
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder()
                .setSigningKey(jwtVerifyingKey)
                .build()
                .parseClaimsJws(token);
        } catch (JwtException e) {
            chain.doFilter(request, response);
            return;
        }

        String uuid = jws.getBody().getSubject();

        // TODO: fetch from db or use object in token?
        Optional<User> optUser = userRepository.findByUuid(uuid);
        if (optUser.isEmpty()) {
            chain.doFilter(request, response);
            return;
        }

        request.setAttribute("user", optUser.get());
        chain.doFilter(request, response);
    }
}
