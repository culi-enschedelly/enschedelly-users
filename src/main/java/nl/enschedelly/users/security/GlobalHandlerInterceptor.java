package nl.enschedelly.users.security;


import nl.enschedelly.users.exception.AdminRequiredException;
import nl.enschedelly.users.exception.AuthenticationRequiredException;
import nl.enschedelly.users.model.User;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GlobalHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            RequiresAuthentication requiresAuthentication = handlerMethod.getMethodAnnotation(RequiresAuthentication.class);
            RequiresAdmin requiresAdmin = handlerMethod.getMethodAnnotation(RequiresAdmin.class);
            if (requiresAuthentication != null || requiresAdmin != null) {
                if (request.getAttribute("user") == null) {
                    throw new AuthenticationRequiredException();
                }

                if (requiresAdmin != null) {
                    User user = (User) request.getAttribute("user");
                    if (!user.getIsAdmin()) {
                        throw new AdminRequiredException();
                    }
                }
            }
        }
        return true;
    }
}
