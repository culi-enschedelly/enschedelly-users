package nl.enschedelly.users.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.*;
import java.security.spec.*;
import java.util.Base64;

@Configuration
public class JwtConfig {
    
    @Value("${jwt.key.private}")
    private String jwtPrivateKey;
    
    @Value("${jwt.key.public}")
    private String jwtPublicKey;

    @Bean
    public PrivateKey jwtSigningKey() throws GeneralSecurityException {
        String key = jwtPrivateKey
            .replace("-----BEGIN PRIVATE KEY-----", "")
            .replace("-----END PRIVATE KEY-----", "")
            .replace("\n", "");

        byte[] decodedKey = Base64.getDecoder().decode(key);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(decodedKey));
    }

    @Bean
    public PublicKey jwtVerifyingKey() throws GeneralSecurityException {
        String key = jwtPublicKey
            .replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", "")
            .replace("\n", "");

        byte[] decodedKey = Base64.getDecoder().decode(key);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        return keyFactory.generatePublic(new X509EncodedKeySpec(decodedKey));
    }
}
