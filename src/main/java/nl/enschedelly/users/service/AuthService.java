package nl.enschedelly.users.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import nl.enschedelly.users.dto.request.LoginRequestDto;
import nl.enschedelly.users.dto.response.TokenResponseDto;
import nl.enschedelly.users.dto.response.UserResponseDto;
import nl.enschedelly.users.exception.IncorrectCredentialsException;
import nl.enschedelly.users.model.User;
import nl.enschedelly.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.PrivateKey;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PrivateKey jwtSigningKey;

    private final PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();

    public TokenResponseDto login(LoginRequestDto loginRequestDto) throws IncorrectCredentialsException {
        User user = userRepository.findByEmail(loginRequestDto.getEmail())
            .orElseThrow(IncorrectCredentialsException::new);

        if (!passwordEncoder.matches(loginRequestDto.getPassword(), user.getPassword())) {
            throw new IncorrectCredentialsException();
        }

        return new TokenResponseDto(
            getTokenFor(user)
        );
    }

    public User createSuperuser(String firstName, String lastName, String email, String password, boolean isAdmin) {
        User user = new User()
            .setFirstName(firstName)
            .setLastName(lastName)
            .setEmail(email)
            .setPassword(passwordEncoder.encode(password))
            .setUuid(UUID.randomUUID().toString())
            .setJoinedAt(new Date().getTime() / 1000)
            .setIsAdmin(isAdmin);

        return userRepository.save(user);
    }

    private String getTokenFor(User user) {
        Calendar expiration = Calendar.getInstance();
        expiration.add(Calendar.MINUTE, 180);

        return Jwts.builder()
            .setHeaderParam("typ", "JWT")
            .setIssuer("enschedelly-users")
            .setSubject(user.getUuid())
            .setExpiration(expiration.getTime())
            .setIssuedAt(new Date())
            .claim("user", UserResponseDto.from(user))
            .signWith(jwtSigningKey, SignatureAlgorithm.ES256)
            .compact();
    }
}

