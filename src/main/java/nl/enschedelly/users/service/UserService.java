package nl.enschedelly.users.service;

import nl.enschedelly.users.dto.request.UserRequestDto;
import nl.enschedelly.users.dto.response.CompactUserResponseDto;
import nl.enschedelly.users.dto.response.UserResponse;
import nl.enschedelly.users.dto.response.UserResponseDto;
import nl.enschedelly.users.exception.AlreadyExistsException;
import nl.enschedelly.users.exception.ModelNotFoundException;
import nl.enschedelly.users.model.User;
import nl.enschedelly.users.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private static final String MODEL_NAME = "user";

    @Autowired
    private UserRepository repository;

    public List<? extends UserResponse> getAllUsers(Boolean pending, Boolean compact) {
        Iterable<User> users;
        if (pending) {
            users = repository.findAllByJoinedAt(null);
        } else {
            users = repository.findAll();
        }

        if (compact) {
            return Streamable.of(users)
                .map(CompactUserResponseDto::from)
                .toList();
        } else {
            return Streamable.of(users)
                .map(UserResponseDto::from)
                .toList();
        }
    }

    public UserResponseDto getUser(String uuid) throws ModelNotFoundException {
        return repository.findByUuid(uuid)
            .map(UserResponseDto::from)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));
    }

    public UserResponseDto createUser(UserRequestDto request) throws AlreadyExistsException {
        if (repository.existsByEmail(request.getEmail())) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        User user = new User()
            .setUuid(UUID.randomUUID().toString())
            .setFirstName(StringUtils.capitalize(request.getFirstName().toLowerCase()))
            .setLastName(StringUtils.capitalize(request.getLastName().toLowerCase()))
            .setEmail(request.getEmail().toLowerCase())
            .setPassword(new Argon2PasswordEncoder().encode(request.getPassword()))
            .setJoinedAt(null)
            .setIsAdmin(false);

        return UserResponseDto.from(
            repository.save(user)
        );
    }

    public UserResponseDto updateUser(String uuid, UserRequestDto request) throws ModelNotFoundException, AlreadyExistsException {
        User user = repository.findByUuid(uuid)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        if (repository.existsByEmailAndUuidNot(request.getEmail(), uuid)) {
            throw new AlreadyExistsException(MODEL_NAME);
        }

        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());

        return UserResponseDto.from(
            repository.save(user)
        );
    }

    public UserResponseDto approveUser(String uuid) throws ModelNotFoundException {
        User user = repository.findByUuid(uuid)
            .orElseThrow(() -> new ModelNotFoundException(MODEL_NAME));

        user.setJoinedAt(System.currentTimeMillis() / 1000L);

        return UserResponseDto.from(
            repository.save(user)
        );
    }

    public void deleteUser(String uuid) throws ModelNotFoundException {
        if (!repository.existsByUuid(uuid)) {
            throw new ModelNotFoundException(MODEL_NAME);
        }
        repository.deleteByUuid(uuid);
    }

    public void denyUser(String uuid) throws ModelNotFoundException {
        if (!repository.existsByUuid(uuid)) {
            throw new ModelNotFoundException(MODEL_NAME);
        }
        // TODO: send email to denied user.
        repository.deleteByUuid(uuid);
    }

}
