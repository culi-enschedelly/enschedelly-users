package nl.enschedelly.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnschedellyUsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnschedellyUsersApplication.class, args);
    }

}
