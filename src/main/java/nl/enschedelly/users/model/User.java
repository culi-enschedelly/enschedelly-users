package nl.enschedelly.users.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String uuid;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String email;

    private String password;

    private Long joinedAt;

    private Boolean isAdmin;

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "author")
//    private Set<Recipe> recipes;

    public boolean isActive() {
        return joinedAt != null;
    }
}

