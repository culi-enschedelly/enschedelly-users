package nl.enschedelly.users.repository;

import nl.enschedelly.users.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    boolean existsByEmail(String email);

    boolean existsByUuid(String uuid);

    boolean existsByEmailAndUuidNot(String email, String uuid);

    Optional<User> findByEmail(String email);

    Optional<User> findByUuid(String uuid);

    List<User> findAllByJoinedAt(Long joinedAt);

    void deleteByUuid(String uuid);
}

