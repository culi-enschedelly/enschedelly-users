CREATE TABLE `users` (
    `id` BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `uuid` VARCHAR(36) UNIQUE NOT NULL,
    `first_name` VARCHAR(100) NOT NULL,
    `last_name` VARCHAR(100) NOT NULL,
    `email` VARCHAR(100) UNIQUE NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `image_url` VARCHAR(511),
    `joined_at` BIGINT UNSIGNED,
    `is_admin` BOOLEAN DEFAULT 0,

    INDEX(`uuid`),
    INDEX(`email`)
);